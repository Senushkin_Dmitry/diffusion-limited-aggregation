#include<iostream>
#include <cstdlib> 
#include <vector>
#include <random>
#include <fstream>
#include <string>
#include <ctime>
using namespace std;

const float eps = 2;
const float A=200;
const int iteration = 300;
const int start_num_particles =1000;

float randomNumber(const float dist);

class integrin
{
public:

	integrin()
	{
		for(auto i = 0; i<2; i++)
		{
			//srand(time(NULL));
			rvec[i] = randomNumber(A);
			vvec[i] = randomNumber(2);
			flag_const = false;
			flag_merger = false;
		}
	}
	~integrin()
	{
	}

	float rvec[2];
	float vvec[2];
	bool flag_const;
	bool flag_merger;


	void increase()
	{
		rvec[0] += vvec[0];
		rvec[1] += vvec[1];
		reInit();
	}
	void reInit()
	{   float g;
	g=randomNumber(2);
	if(g>1)
		{vvec[0] = randomNumber(2);
	vvec[1] = randomNumber(2);}
	else
		{vvec[0] = randomNumber(2)*(-1);
	vvec[1] = randomNumber(2)*(-1);}
	}
	

	bool operator!=(integrin a)
	{
		return !(rvec[0] == a.rvec[0] && rvec[1] == a.rvec[1]);
	}
	integrin operator=(int a)
	{
		for(auto i = 0; i<2; i++)
		{
			rvec[i] = a;
			vvec[i] = a;
			flag_const = false;
		}
		return *this;
	}
	bool operator!=(int a)
	{
		return !(rvec[0] == a && rvec[1] == a);
		
	}
	integrin & operator=(const integrin & I)
	{
		for(auto i = 0; i<2; i++)
		{
			rvec[i] = I.rvec[i];
			vvec[i] = I.vvec[i];
		}
		flag_const = I.flag_const;
		flag_merger = I.flag_merger;
		return *this;
	}
	integrin(const integrin & I)
	{
		for(auto i = 0; i<2; i++)
		{
			rvec[i] = I.rvec[i];
			vvec[i] = I.vvec[i];
		}
		flag_const = I.flag_const;
		flag_merger = I.flag_merger;
	}
};

float randomNumber(const float dist)  // ���������� ��������� ��������� ����� ��� ������� �������� [0,hi]
{

	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<float> dis(0, dist);
	return dis(gen);
}

void check(integrin* a, vector<integrin*> * container)
{
	
	for(auto i = 0; i<container->size(); ++i)
	{
		if(a!=container->at(i) && (container->at(i)->flag_const || container->at(i)->flag_merger))
		{
			float r=sqrt((a->rvec[0] - container->at(i)->rvec[0])*(a->rvec[0] - container->at(i)->rvec[0])+(a->rvec[1] - container->at(i)->rvec[1])*(a->rvec[1] - container->at(i)->rvec[1]));
			if(r<=eps)
			{
				a->flag_merger = true;
			}
		}
	}
}

void createGrid(vector<integrin*> * container)
{
	for (auto i = 0; i <= A;i+=50)
	{
		for(auto j = 0; j <= A;j+=50)
		{
			integrin* newIntergin = new integrin;
			newIntergin->rvec[0] = i;
			newIntergin->rvec[1] = j;
			newIntergin->vvec[0] = 0;
			newIntergin->vvec[1] = 0;
			newIntergin->flag_const = true;
			container->push_back(newIntergin);

			
		}

		
	}
}
bool check2(integrin* a)
{
	
			if(	a->rvec[0] >=A ||
				a->rvec[1] >=A||
				a->rvec[0]<0||
				a->rvec[1]<0)
			{
				return false;
			}
			return true;
}

	/*vector<integrin*> temp(*container);
	container->clear();
	for(auto i = 0; i < temp.size(); i++)
	{
		if(temp.at(i) != NULL)
			container->push_back(temp.at(i));
	}*/

int main()
{
	vector<integrin*> *container = new vector<integrin*>;
	createGrid(container);
	
	
	for(auto i = 0; i < start_num_particles; i++)
	container->push_back(new integrin());
			
	ofstream fout("13.txt");
	for(auto t = 0; t < iteration; t++)
	{
		cout<<t<<endl;
		for(auto i = 0; i < container->size(); i++)
		{ 
			//createGri(container->at(i),container);
				//cout << t <<" "<<i<<" "<< container->at(i)->rvec[0] << " " << container->at(i)->rvec[1] << "\n";
				fout<< t << " " <<i<<" "<< container->at(i)->rvec[0] << " " << container->at(i)->rvec[1] <<" ";
				if (container->at(i)->flag_const)
					fout<<1;
				else
				if (container->at(i)->flag_merger)
					fout<<2;
				else
					fout<<0;
				fout<<endl;
		}
					
		for(auto i = 0; i < container->size(); i++)
		{	
			if(!container->at(i)->flag_const && !container->at(i)->flag_merger)
				{
					integrin temp;
					temp.rvec[0]=container->at(i)->rvec[0];
					temp.rvec[1]=container->at(i)->rvec[1];
					temp.vvec[0]=container->at(i)->vvec[0];
					temp.vvec[1]=container->at(i)->vvec[1];
					temp.increase();
					if (check2(&temp)) 
					{
						
						container->at(i)->rvec[0]=temp.rvec[0];
						container->at(i)->rvec[1]=temp.rvec[1];
						container->at(i)->vvec[0]=temp.vvec[0];
						container->at(i)->vvec[1]=temp.vvec[1];
						check ( container->at(i), container);
					}
					else 
					{
						container->at(i)->vvec[0]=-container->at(i)->vvec[0];
						container->at(i)->vvec[1]=-container->at(i)->vvec[1];
					}

				}
			}
	}
	fout.close();
	
	system("pause");
	
}
